package com.catch22.savvy.backgroundProcesses;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.catch22.savvy.R;
import com.catch22.savvy.activities.PopupActivity;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class BackgroundService extends Service
{
    private NotificationManager notificationManager;
    public static String lastActivePackage;
    private Intent openPopupActivityIntent;
    public static boolean isNetWorkAvailable;

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        openPopupActivityIntent = new Intent(this, PopupActivity.class);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        new Thread() //run a thread every one second to get the fore Ground's activity name and check is internet is available
        {
            @Override
            public void run()
            {
                    while(true)
                    {
                        String temporaryName = getUsageStatsForegroundActivityName(); //the last activity name
                        if(!temporaryName.equals(lastActivePackage) && !temporaryName.contains("savvy")) //if the new last activity name isn't equal to the last activity name and the last activity name is not from savvy
                            lastActivePackage = temporaryName; //update the last activity name

                        isNetWorkAvailable = isNetworkAvailable();

                        try
                        {
                            sleep(1000);
                        } catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
            }
        }.start();

        showNotification();

        return START_STICKY;
    }

    /**
     * Get the name of the last opened Package
     * @return the name of the last opened package
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private String getUsageStatsForegroundActivityName()
    {
        String topActivity = null;
        try
        {
            UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
            long endTime = System.currentTimeMillis();
            long beginTime = endTime - 1000 * 60;

            // result

            // We get usage stats for the last minute
            List<UsageStats> stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, beginTime, endTime);

            // Sort the stats by the last time used
            if(stats != null)
            {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<>();
                for(UsageStats usageStats : stats)
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);

                if(!mySortedMap.isEmpty())
                    topActivity = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        if(topActivity != null)
        {
            return topActivity;
        }
        else
            return ""+-1;

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        notificationManager.cancelAll();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showNotification()
    {
        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, openPopupActivityIntent, 0);

        // Set the info for the views that show in the notification panel.
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)  // the status icon
                .setTicker("Service Running")  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setContentTitle(("Savvy"))  // the label of the entry
                .setContentText("What can I help you with?")  // the contents of the entry
                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                .setVisibility(-1)                    //Make the Notification invisible on the home screen
                .setOngoing(true)
                .build();

        // Send the notification.
        notificationManager.notify(5, notification);
    }

    /**
     * Checks if user's phone is connected to the internet via either Wifi or 2G, 3G, 4G
     * @return
     */
    private boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}