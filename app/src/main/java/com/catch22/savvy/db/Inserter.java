package com.catch22.savvy.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import static com.catch22.savvy.db.DatabaseHelper.*;

public class Inserter
{
    private static DatabaseHelper databaseHelper;
    private static SQLiteDatabase db;

    public static void insertAllEntries(DatabaseHelper databaseHelperParameterPass)
    {
        databaseHelper = databaseHelperParameterPass;
        db = databaseHelper.getWritableDatabase();

        Log.d("Database", "Inserting");

        deleteAllEntries(db);

        insertIntoApps(1, "whatsapp");
        insertIntoApps(2, "messaging");

        insertIntoApps(3, "dialer");
        insertIntoApps(4, "launcher", "googlequicksearchbox");

        insertIntoActions(1, "leave", "exit");
        insertIntoActions(3, "setup", "initialize", "initialise");
        insertIntoActions(4, "make", "create");
        insertIntoActions(6, "view", "see", "open", "check");
        insertIntoActions(7, "write");
        insertIntoActions(8, "use", "utilize", "utilise");
        insertIntoActions(9, "delete", "remove");
        insertIntoActions(10, "import");
        insertIntoActions(12, "decrease", "reduce", "lower", "increase", "higher");
        insertIntoActions(14, "change", "update", "modify", "set");
        insertIntoActions(17, "sort", "order");
        insertIntoActions(18, "select", "enable", "disable", "deselect");
        insertIntoActions(19, "get");
        insertIntoActions(22, "search", "find");
        insertIntoActions(25, "add", "insert");
        insertIntoActions(26, "start", "begin", "terminate", "stop", "end");
        insertIntoActions(27, "load");
        insertIntoActions(28, "save");
        insertIntoActions(29, "mute", "sound off", "no sound");
        insertIntoActions(30, "switch", "toggle");
        insertIntoActions(32, "dial", "call");
        insertIntoActions(33, "invite");
        insertIntoActions(38, "send", "attach");
        insertIntoActions(39, "star");
        insertIntoActions(38, "promote");
        insertIntoActions(42, "unstar");

        insertIntoTasks(1, "message", "sms", "text");
        insertIntoTasks(2, "wallpaper", "back ground", "background");
        insertIntoTasks(3, "tone", "sound", "ringtone", "ring tone", "audio");
        insertIntoTasks(4, "photo", "photograph", "picture", "pic", "pix", "image", "video", "vid");
        insertIntoTasks(5, "doc", "document", "file");
        insertIntoTasks(7, "dial speed", "speed dial", "speeddial", "speedial", "quick dial");
        insertIntoTasks(9, "bluetooth", "blue tooth");
        insertIntoTasks(10, "balance", "money", "credit");
        insertIntoTasks(12, "emoticon", "face", "emoji", "cartoon", "smiley");
        insertIntoTasks(13, "delivery report");
        insertIntoTasks(14, "history", "log");
        insertIntoTasks(16, "starred");
        insertIntoTasks(19, "contact", "person", "friend", "number", "name", "someone");
        insertIntoTasks(22, "group");
        insertIntoTasks(24, "data", "2g", "3g", "4g");
        insertIntoTasks(26, "dp", "profile picture");
        insertIntoTasks(28, "administrator", "moderator", "admin");
        insertIntoTasks(29, "silent", "vibrate", "vibration");
        insertIntoTasks(33, "web");
        insertIntoTasks(35, "internet");
        insertIntoTasks(36, "account", "profile");
        insertIntoTasks(38, "convo", "conversation", "chat");
        insertIntoTasks(41, "sim");
        insertIntoTasks(42, "status");
        insertIntoTasks(43, "brightness", "light", "backlight", "back light");
        insertIntoTasks(46, "wifi");

        insertIntoQueries(1, 4, 22, 1);
        insertIntoQueries(1, 14, 19, 2);
        insertIntoQueries(1, 4, 19, 3);
        insertIntoQueries(1, 25, 19, 3);
        insertIntoQueries(1, 6, 19, 4);
        insertIntoQueries(1, 14, 26, 5);
        insertIntoQueries(1, 6, 26, 6);
        insertIntoQueries(1, 9, 36, 7);
        insertIntoQueries(1, 4, 28, 47);
        insertIntoQueries(1, 38, 28, 47);
        insertIntoQueries(1, 29, 22, 9);
        insertIntoQueries(1, 1, 22, 10);
        insertIntoQueries(1, 33, 19, 11);
        insertIntoQueries(1, 14, 19, 12);
        insertIntoQueries(1, 14, 3, 15);
        insertIntoQueries(1, 39, 1, 16);
        insertIntoQueries(1, 6, 16, 17);
        insertIntoQueries(1, 42, 1, 18);
        insertIntoQueries(1, 14, 42, 19);
        insertIntoQueries(1, 8, 33, 20);
        insertIntoQueries(1, 38, 4, 38);
        insertIntoQueries(1, 38, 19, 38);
        insertIntoQueries(1, 38, 5, 38);
        insertIntoQueries(1, 14, 2, 39);
        insertIntoQueries(1, 9, 1, 40);
        insertIntoQueries(3, 4, 19, 21);
        insertIntoQueries(3, 9, 7, 22);
        insertIntoQueries(3, 6, 14, 23);
        insertIntoQueries(3, 32, 19, 25);
        insertIntoQueries(3, 10, 41, 26);
        insertIntoQueries(3, 10, 19, 26);
        insertIntoQueries(3, 14, 3, 27);
        insertIntoQueries(3, 17, 19, 28);
        insertIntoQueries(3, 18, 29, 30);
        insertIntoQueries(3, 6, 10, 31);
        insertIntoQueries(2, 4, 1, 32);
        insertIntoQueries(2, 7, 1, 32);
        insertIntoQueries(2, 38, 1, 32);
        insertIntoQueries(2, 22, 38, 33);
        insertIntoQueries(2, 7, 19, 35);
        insertIntoQueries(2, 38, 19, 35);
        insertIntoQueries(2, 38, 12, 36);
        insertIntoQueries(2, 38, 4, 37);
        insertIntoQueries(2, 38, 19, 37);
        insertIntoQueries(2, 38, 3, 37);
        insertIntoQueries(2, 18, 13, 41);
        insertIntoQueries(2, 19, 13, 41);
        insertIntoQueries(2, 14, 3, 42);
        insertIntoQueries(4, 14, 2, 43);
        insertIntoQueries(4, 14, 2, 43);
        insertIntoQueries(4, 14, 43, 44);
        insertIntoQueries(4, 12, 43, 44);
        insertIntoQueries(4, 26, 24, 45);
        insertIntoQueries(4, 30, 24, 45);
        insertIntoQueries(4, 26, 46, 45);
        insertIntoQueries(4, 30, 46, 45);
        insertIntoQueries(4, 26, 35, 45);
        insertIntoQueries(4, 30, 35, 45);
        insertIntoQueries(4, 30, 9, 46);
        insertIntoQueries(4, 26, 9, 46);

        insertIntoSolutions("Open WhatsApp and go to the Chats screen." +
                "At the top of the Chats screen, tap the 3-dot button at the upper right corner." + "Type in a subject or title. ... " +
                "Add group participants by selecting (+), or by typing the name of the contact. " +
                "Tap Create to finish creating the group.", 1);

        insertIntoSolutions("Tap the 3-dot button at the upper right corner. " + "Go to Settings > Account > Change Number. ", 2);

        insertIntoSolutions("Press the new conversation button at the top right corner beside the 3-dot button " + "Select a contact by scrolling through the list ", 3);

        insertIntoSolutions("Go to the chat screen(main screen). " + "Swipe right. ", 4);

        insertIntoSolutions("Tap the 3-dot button at the upper right corner. " + "Go to Settings > Profile " + "Click on the Edit icon (Pencil)", 5);

        insertIntoSolutions("To view DP go to Settings > Profile " + "To make further changes click on the edit button ", 6);

        insertIntoSolutions("To delete your account go to Settings > Account > Delete my Account ", 7);

        insertIntoSolutions("To create a group click on 3-dot menu > New Group ", 8);

        insertIntoSolutions("To mute the group click on the group desired to be muted " + "Click on the 3-dot menu > mute ", 9);

        insertIntoSolutions("Long click on the group you want to exit > Exit group ", 10);

        insertIntoSolutions("Go to the contacts tab > Scroll down to the bottom " + "Select the person whom you want to send an invite ", 11);

        insertIntoSolutions("To change your Whatsapp name go to Settings > Profile " + "Click on the edit button beside you name ", 12);

        insertIntoSolutions("To modify notification tone go to Settings > Notification > Notification tone ", 13);

        insertIntoSolutions("To change the Message tone go to 3-dot menu > Settings " + "In the Notifications > Sound ", 14);

        insertIntoSolutions("Go to a chat and press and hold the particular text you want to star. Press the starred icon at the top ", 16);

        insertIntoSolutions("To view all the starred messages, press the 3-dot button at the top right corner Go to 'Starred messages' ", 17);

        insertIntoSolutions("Go to the list of starred messages by pressing the 3-dot button at the top right corner Press and hold the message you want to remove frm the list Press the crossed star button ", 18);

        insertIntoSolutions("Press the 3-dot button at the top right corner Go to 'status' Press the pen button beside your current status and update it ", 19);

        insertIntoSolutions("Press the 3-dot button at the top right corner Go to 'WhatsApp Web' ", 20);

        insertIntoSolutions("To create a new contact, tap the 3-dot button at the top right corner, beside the mic button Go to 'New Conatact' Fill out Name, Phone and any further data you would like to keep Click the back button to save the contact ", 21);

        insertIntoSolutions("To remove a speed dial, press and hold the icon of the contact you wish to remove While holding it, drag it to the 'remove' option below the search field ", 22);

        insertIntoSolutions("To view the call history, simply swipe right or press on 'Recents' option Swiping once more will direct you to your Contacts list ", 23);

        insertIntoSolutions("To call someone on, either press the name on the current screen If the name is not present, press the 'Contacts' option and scroll through your contacts to find the person you want to call You could also type the name of the person in search bar by clicking on the white space on top If you wish to call a number not saved in your contacts, press the blue button with 9 dots on the bottom center of the screen ", 24);

        insertIntoSolutions("To call someone on, either press the name on the current screen If the name is not present, press the 'Contacts' option and scroll through your contacts to find the person you want to call You could also type the name of the person in search bar by clicking on the white space on top If you wish to call a number not saved in your contacts, press the blue button with 9 dots on the bottom center of the screen ", 25);

        insertIntoSolutions("Press the 3-dot button on the top right corner in the white space Go to 'Import/export' Go to 'Import from SIM Card' and click on the Google account ", 26);

        insertIntoSolutions("Press the 3-dot button on the top right corner in the white space Go to Settings > General Click on 'Phone Ringtone'", 27);

        insertIntoSolutions("Press the 3-dot button on the top right corner in the white space Go to Settings > General Click on 'Sort by'", 28);

        insertIntoSolutions("Press the 3-dot button on the top right corner in the white space Go to Settings > General Click on 'Name format'", 29);

        insertIntoSolutions("Press the 3-dot button on the top right corner in the white space Go to Settings > General Check 'Also vibrate for calls'", 30);

        insertIntoSolutions("To check balance " + "IDEA subscribers dial:*121# " + "Vodafone subscribers dial:*111# " + "Airtel subscribers dial:*123# " + "Aircel subscribers dial:*125# " + "TATA DOCOMO subscribers dial:*111# " + "Relaiance subscribers dial:*367# ", 31);

        insertIntoSolutions("Click on New Message icon (has + sign) on top right ", 32);

        insertIntoSolutions("Click on Search icon (Magnifying glass) on top right ", 33);

        insertIntoSolutions("Click on New Message icon (has + sign) on top right ", 35);

        insertIntoSolutions("Long press the Enter key on your keyboard Click on the emoji of your choice Press send", 36);

        insertIntoSolutions("Open a chat. Tap the paperclip at the top of the screen. Choose what you want to send:Choose Document to select a document from your phone.Choose Camera to take a picture with your camera.Choose Gallery to select an existing photo or video from your phone. You can long press to select multiple images.Choose Audio to record a message or send an existing audio from your phone.Choose Location to send your location or a nearby place.Choose Contact to send the information of a contact saved in your phone's address book. Press Send.", 37);

        insertIntoSolutions("Open a chat. Tap the paperclip at the top of the screen. Choose what you want to send: Choose Document to select a document from your phone. Choose Camera to take a picture with your camera. Choose Gallery to select an existing photo or video from your phone. You can long press to select multiple images. Choose Audio to record a message or send an existing audio from your phone. Choose Location to send your location or a nearby place. Choose Contact to send the information of a contact saved in your phone's address book. Press Send.", 38);

        insertIntoSolutions("Open WhatsApp Messenger and tap on the Menu key. Go to Settings > Chat Settings. Tap on Wallpaper. Tap on Android System > Gallery.", 39);

        insertIntoSolutions("Open WhatsApp and go to the chat window with the message you want to delete. Tap and hold on the message. Optionally, tap on more messages to select multiple messages. Tap on the trash can icon on top of the chat screen.", 40);

        insertIntoSolutions("Select the 3 dots at the top of the screen Tap Settings Tap Text Messages Check the box next to Delivery", 41);

        insertIntoSolutions("Select the 3 dots at the top of the screen Tap Settings Scroll down to the Notifications section and tap Sound. Select a new notification sound from the List.", 42);

        insertIntoSolutions("Click and hold the homescreen " + "Choose the Wallpaper option ", 43);

        insertIntoSolutions("Swipe from top, Click on the top right-most icon " + "Move the slider to get the appropriate brightness ", 44);

        insertIntoSolutions("Hold the notification bar on the top on the screen and drag it down Tap the Wifi / Mobile Data option to enable it.", 45);

        insertIntoSolutions("Hold the notification bar on the top on the screen and drag it down Tap the Bluetooth option to enable it.", 46);

    }

    private static void insertIntoApps(int appId, String... nameSynonyms)
    {
        for(String name : nameSynonyms)
            databaseHelper.insertInto(db, APPS_TABLE, new AttributeValuePair(APP_NAME, name), new AttributeValuePair(APP_ID, appId));
    }

    private static void insertIntoTasks(int taskId, String... taskSynonyms)
    {
        for(String task : taskSynonyms)
            databaseHelper.insertInto(db, TASKS_TABLE, new AttributeValuePair(TASK_NAME, task), new AttributeValuePair(TASK_ID, taskId));
    }

    private static void insertIntoActions(int actionId, String... actionSynonyms)
    {
        for(String action : actionSynonyms)
            databaseHelper.insertInto(db, ACTIONS_TABLE, new AttributeValuePair(ACTION_NAME, action), new AttributeValuePair(ACTION_ID, actionId));
    }

    private static void insertIntoQueries(int app, int action, int task, int solution)
    {
        databaseHelper.insertInto(db, QUERIES_TABLE, new AttributeValuePair(QUERY_ACTION, action), new AttributeValuePair(QUERY_APP, app), new AttributeValuePair(QUERY_TASK, task), new AttributeValuePair(QUERY_SOLUTION, solution));
    }

    private static void insertIntoSolutions(String solution, int solutionId)
    {
        databaseHelper.insertInto(db, SOLUTIONS_TABLE, new AttributeValuePair(SOLUTION_NAME, solution), new AttributeValuePair(SOLUTION_ID, solutionId));
    }
}
