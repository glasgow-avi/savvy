package com.catch22.savvy.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;

public class DatabaseHelper extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "savvy.db";
    public static final String
            APPS_TABLE = "apps", APP_ID = "id", APP_NAME = "name",
            ACTIONS_TABLE = "Actions", ACTION_ID = "id", ACTION_NAME = "action",
            SOLUTIONS_TABLE = "Solutions", SOLUTION_ID = "id", SOLUTION_NAME = "solution",
            TASKS_TABLE = "Tasks", TASK_ID = "id", TASK_NAME = "task",
            QUERIES_TABLE = "queries", QUERY_TASK = "task", QUERY_ACTION = "action", QUERY_APP = "app", QUERY_SOLUTION = "solution";

    public DatabaseHelper(Context context, int version)
    {
        super(context, DATABASE_NAME, null, version);
    }

    // Called when no database exists in disk and the helper class needs
    // to create a new one.
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        Log.d("Database", "Created");

        createTable(db, APPS_TABLE, new Column(APP_ID, "integer"), new Column(APP_NAME, "text", "primary key"));
        createTable(db, ACTIONS_TABLE, new Column(ACTION_ID, "integer"), new Column(ACTION_NAME, "text", "primary key"));
        createTable(db, SOLUTIONS_TABLE, new Column(SOLUTION_ID, "integer"), new Column(SOLUTION_NAME, "text", "primary key"));
        createTable(db, TASKS_TABLE, new Column(TASK_ID, "integer"), new Column(TASK_NAME, "text", "primary key"));
        createTable(db, QUERIES_TABLE, new Column(QUERY_ACTION, "integer"), new Column(QUERY_APP, "integer"), new Column(QUERY_TASK, "integer"), new Column(QUERY_SOLUTION, "integer"));
    }

    // Called when there is a database version mismatch meaning that the version
    // of the database on disk needs to be upgraded to the current version.
    @Override
    public void onUpgrade(SQLiteDatabase db, int _oldVersion, int _newVersion)
    {
        // Log the version upgrade.
        Log.w("TaskDBAdapter", "Upgrading from version " + _oldVersion + " to " + _newVersion + ", which will destroy all old data");

        // Upgrade the existing database to conform to the new version. Multiple
        // previous versions can be handled by comparing _oldVersion and _newVersion
        // values.
        // The simplest case is to drop the old table and create a new one.
        Log.d("Database", "Drop Tables");
        db.execSQL("DROP TABLE IF EXISTS " + ACTIONS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + APPS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SOLUTIONS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + QUERIES_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TASKS_TABLE);
    }

    /**
     * Inserts into a table
     * @param tableName The table name
     * @param values An attribute-value pair
     */
    public void insertInto(SQLiteDatabase db, String tableName, AttributeValuePair... values)
    {
        ContentValues contentValues = new ContentValues();

        for(AttributeValuePair value : values)
            if(value.value instanceof String)
                contentValues.put(value.attribute, (String) value.value);
            else
                contentValues.put(value.attribute, (Integer) value.value);

        db.insert(tableName, null, contentValues);
    }

    public static void deleteAllEntries(SQLiteDatabase db)
    {
        db.execSQL("delete from " + APPS_TABLE);
        db.execSQL("delete from " + ACTIONS_TABLE);
        db.execSQL("delete from " + TASKS_TABLE);
        db.execSQL("delete from " + QUERIES_TABLE);
        db.execSQL("delete from " + SOLUTIONS_TABLE);
    }

    private final void createTable(SQLiteDatabase db, String tableName, Column... columns)
    {
        String query = "create table " + tableName + "(";
        for(Column column : columns)
            query += column.name + " " + column.dataType + " " + column.constraints + ", ";

        query = query.substring(0, query.length() - 2);

        query += ");";

        db.execSQL(query);
    }

    protected static class Column
    {
        public String name;
        public String dataType;
        public String constraints;

        public Column(String name, String dataType)
        {
            this.name = name;
            this.dataType = dataType;
            this.constraints = "";
        }

        public Column(String name, String dataType, String constraints)
        {
            this.name = name;
            this.dataType = dataType;
            this.constraints = constraints;
        }
    }

    protected static class AttributeValuePair
    {
        public String attribute;
        public Object value;

        public AttributeValuePair(String attribute, Object value)
        {
            this.attribute = attribute;
            this.value = value;
        }
    }

    /**
     * Does a table have a specific entry in a certain column?
     * Performs SELECT * from tableName WHERE columnName = 'key'
     * @param tableName The table name
     * @param columnName The attribute name
     * @param key The search key that you are looking for
     * @return If the above query returns an empty set, it returns null
     *  If the above query returns a non empty set, it returns the first id
     */

    public static int getId(SQLiteDatabase db, String tableName, String columnName, String key)
    {
        Log.d("Database", "select id from " + tableName + " where " + columnName + " = '" + key + "';");
        Cursor c = db.rawQuery("select id from " + tableName + " where " + columnName + " = '" + key + "'", null);

        Log.d("Database", "Successful query with result set of cardinality " + c.getCount());
        if(c.getCount() == 0)
            return 0;

        c.moveToNext();
        return c.getInt(0);
    }

    public static HashSet<Integer> getFillerQueries(SQLiteDatabase db, int appId, int taskId, int actionId)
    {
        HashSet<Integer> res = new HashSet<>();

        String query = null;
        if(taskId == 0)
            query = "select " + QUERY_TASK + " from " + QUERIES_TABLE + " where " + QUERY_ACTION + " = " + actionId + " and " + QUERY_APP + " = " + appId;
        if(actionId == 0)
            query = "select " + QUERY_ACTION + " from " + QUERIES_TABLE + " where " + QUERY_TASK + " = " + taskId + " and " + QUERY_APP + " = " + appId;

        Cursor c = db.rawQuery(query, null);
        while(c.moveToNext())
            res.add(c.getInt(0));

        return res;
    }

    public static String getCorrespondingName(SQLiteDatabase db, int id, String tableName, String columnName)
    {
        Cursor c = db.rawQuery("select " + columnName + " from " + tableName + " where id = " + id, null);

        c.moveToNext();
        return c.getString(0);
    }

    public static final String ERROR_CODE = "sookitty";

    public static void logTables(SQLiteDatabase db)
    {
        Cursor c = db.rawQuery("select " + ACTION_NAME + " from " + ACTIONS_TABLE, null);
        while(c.moveToNext())
            Log.d(ACTIONS_TABLE, c.getString(0));

        c = db.rawQuery("select " + TASK_NAME + " from " + TASKS_TABLE, null);
        while(c.moveToNext())
            Log.d(ACTIONS_TABLE, c.getString(0));

        c = db.rawQuery("select " + APP_NAME + " from " + APPS_TABLE, null);
        while(c.moveToNext())
            Log.d(ACTIONS_TABLE, c.getString(0));
    }

    public static String getSolution(SQLiteDatabase db, int actionId, int taskId, int appId)
    {
        Cursor c = db.rawQuery("select " + SOLUTION_NAME + " from " + SOLUTIONS_TABLE + " where " + SOLUTION_ID + " in ( select " + QUERY_SOLUTION + " from " + QUERIES_TABLE + " where " + QUERY_TASK + " = " + taskId + " and " + QUERY_APP + " = " + appId + " and " + QUERY_ACTION + " = " + actionId + " )", null);

        if(c.getCount() == 0)
            return ERROR_CODE;

        c.moveToNext();
        return c.getString(0);
    }

    public static String getSolution(int id)
    {
        switch (id)
        {
            case 1:
                return "Open WhatsApp and go to the Chats screen.\n" +
                        "At the top of the Chats screen, tap the 3-dot button at the upper right corner.\n" +
                        "Type in a subject or title. ...\n" +
                        "Add group participants by selecting (+), or by typing the name of the contact.\n" +
                        "Tap Create to finish creating the group.";

            case 2:
                return "Tap the 3-dot button at the upper right corner.\n" +
                        "Go to Settings > Account > Change Number.\n";

            case 3:
                return "Press the new conversation button at the top right corner beside the 3-dot button\n" +
                        "Select a contact by scrolling through the list\n";

            case 4:
                return "Go to the chat screen(main screen).\n" +
                        "Swipe right.\n";

            case 5:
                return "Tap the 3-dot button at the upper right corner.\n" +
                        "Go to Settings > Profile \n" +
                        "Click on the Edit icon (Pencil)";

            case 6:
                return "To view DP go to Settings > Profile\n" +
                        "To make further changes click on the edit button\n";

            case 7:
                return "To delete your account go to Settings > Account > Delete my Account\n";

            case 8:
                return "To create a group click on 3-dot menu > New Group\n";

            case 9:
                return "To mute the group click on the group desired to be muted\n" +
                        "Click on the 3-dot menu > mute\n";

            case 10:
                return "Long click on the group you want to exit > Exit group\n";

            case 11:
                return "Go to the contacts tab > Scroll down to the bottom\n" +
                        "Select the person whom you want to send an invite\n";

            case 12:
                return "To change your Whatsapp name go to Settings > Profile\n" +
                        "Click on the edit button beside you name\n";

            case 13:
                return "To modify notification tone go to Settings > Notification > Notification tone\n";

            case 14:
                return "To change the Message tone go to 3-dot menu > Settings\n" +
                        "In the Notifications > Sound\n";

            case 16:
                return "Go to a chat and press and hold the particular text you want to star.\nPress the starred icon at the top\n";

            case 17:
                return "To view all the starred messages, press the 3-dot button at the top right corner\nGo to 'Starred messages'\n";

            case 18:
                return "Go to the list of starred messages by pressing the 3-dot button at the top right corner\nPress and hold the message you want to remove frm the list\nPress the crossed star button\n";

            case 19:
                return "Press the 3-dot button at the top right corner\nGo to 'status'\nPress the pen button beside your current status and update it\n";

            case 20:
                return "Press the 3-dot button at the top right corner\nGo to 'WhatsApp Web'\n";

            case 21:
                return "To create a new contact, tap the 3-dot button at the top right corner, beside the mic button\n Go to 'New Conatact'\nFill out Name, Phone and any further data you would like to keep\n Click the back button to save the contact\n";

            case 22:
                return "To remove a speed dial, press and hold the icon of the contact you wish to remove\nWhile holding it, drag it to the 'remove' option below the search field\n";

            case 23:
                return "To view the call history, simply swipe right or press on 'Recents' option\nSwiping once more will direct you to your Contacts list\n";

            case 24:
                return "To call someone on, either press the name on the current screen\nIf the name is not present, press the 'Contacts' option and scroll through your contacts to find the person you want to call\nYou could also type the name of the person in search bar by clicking on the white space on top\nIf you wish to call a number not saved in your contacts, press the blue button with 9 dots on the bottom center of the screen\n";

            case 25:
                return "To call someone on, either press the name on the current screen\nIf the name is not present, press the 'Contacts' option and scroll through your contacts to find the person you want to call\nYou could also type the name of the person in search bar by clicking on the white space on top\nIf you wish to call a number not saved in your contacts, press the blue button with 9 dots on the bottom center of the screen\n";

            case 26:
                return "Press the 3-dot button on the top right corner in the white space\nGo to 'Import/export'\nGo to 'Import from SIM Card' and click on the Google account\n";

            case 27:
                return "Press the 3-dot button on the top right corner in the white space\nGo to Settings > General\nClick on 'Phone Ringtone'";

            case 28:
                return "Press the 3-dot button on the top right corner in the white space\nGo to Settings > General\nClick on 'Sort by'";

            case 29:
                return "Press the 3-dot button on the top right corner in the white space\nGo to Settings > General\nClick on 'Name format'";

            case 30:
                return "Press the 3-dot button on the top right corner in the white space\nGo to Settings > General\nCheck 'Also vibrate for calls'";

            case 31:
                return "To check balance\n" +
                        "IDEA subscribers dial:*121#\n" +
                        "Vodafone subscribers dial:*111#\n" +
                        "Airtel subscribers dial:*123#\n" +
                        "Aircel subscribers dial:*125#\n" +
                        "TATA DOCOMO subscribers dial:*111#\n" +
                        "Relaiance subscribers dial:*367#\n";

            case 32:
                return "Click on New Message icon (has + sign) on top right\n";

            case 33:
                return "Click on Search icon (Magnifying glass) on top right\n";

            case 35:
                return "Click on New Message icon (has + sign) on top right\n";

            case 36:
                return "Long press the Enter key on your keyboard\nClick on the emoji of your choice\nPress send";

            case 37:
                return "Open a chat.\nTap the paperclip at the top of the screen.\nChoose what you want to send:\nChoose Document to select a document from your phone.\nChoose Camera to take a picture with your camera.\nChoose Gallery to select an existing photo or video from your phone. You can long press to select multiple images.\nChoose Audio to record a message or send an existing audio from your phone.\nChoose Location to send your location or a nearby place.\nChoose Contact to send the information of a contact saved in your phone's address book.\nPress Send.";

            case 38:
                return "Open a chat.\nTap the paperclip at the top of the screen.\nChoose what you want to send:\nChoose Document to select a document from your phone.\nChoose Camera to take a picture with your camera.\nChoose Gallery to select an existing photo or video from your phone. You can long press to select multiple images.\nChoose Audio to record a message or send an existing audio from your phone.\nChoose Location to send your location or a nearby place.\nChoose Contact to send the information of a contact saved in your phone's address book.\nPress Send.";

            case 39:
                return "Open WhatsApp Messenger and tap on the Menu key.\nGo to Settings > Chat Settings.\nTap on Wallpaper.\nTap on Android System > Gallery.";

            case 40:
                return "Open WhatsApp and go to the chat window with the message you want to delete.\nTap and hold on the message.\nOptionally, tap on more messages to select multiple messages.\nTap on the trash can icon on top of the chat screen.";

            case 41:
                return "Select the 3 dots at the top of the screen\nTap Settings\nTap Text Messages\nCheck the box next to Delivery";

            case 42:
                return "Select the 3 dots at the top of the screen\nTap Settings\nScroll down to the Notifications section and tap Sound.\nSelect a new notification sound from the List.";

            case 43:
                return "Click and hold the homescreen\n" + "Choose the Wallpaper option\n";

            case 44:
                return "Swipe from top, Click on the top right-most icon\n" +
                        "Move the slider to get the appropriate brightness\n";

            case 45:
                return "Hold the notification bar on the top on the screen and drag it down\nTap the Wifi / Mobile Data option to enable it.";

            case 46:
                return "Hold the notification bar on the top on the screen and drag it down\nTap the Bluetooth option to enable it.";

        }

        return ERROR_CODE;
    }
}
