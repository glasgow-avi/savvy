package com.catch22.savvy.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashSet;

import static com.catch22.savvy.activities.SplashScreen.databaseHelper;
import static com.catch22.savvy.db.DatabaseHelper.ACTIONS_TABLE;
import static com.catch22.savvy.db.DatabaseHelper.ACTION_NAME;
import static com.catch22.savvy.db.DatabaseHelper.APPS_TABLE;
import static com.catch22.savvy.db.DatabaseHelper.APP_NAME;
import static com.catch22.savvy.db.DatabaseHelper.ERROR_CODE;
import static com.catch22.savvy.db.DatabaseHelper.TASKS_TABLE;
import static com.catch22.savvy.db.DatabaseHelper.TASK_NAME;
import static com.catch22.savvy.db.DatabaseHelper.getCorrespondingName;
import static com.catch22.savvy.db.DatabaseHelper.getFillerQueries;
import static com.catch22.savvy.db.DatabaseHelper.getId;
import static com.catch22.savvy.db.DatabaseHelper.getSolution;

public class StringHandler
{
    public static class Query
    {
        public String task, action;
        public String solution;

        public Query(String task, String action, String solution)
        {
            this.task = task;
            this.action = action;
            this.solution = solution;
        }

        private String toProperCase(String s)
        {
            return Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }

        @Override
        public String toString()
        {
            return toProperCase(action) + " " + task;
        }
    }

    public static ArrayList<Query> stripQuery(Context context, String query)
    {
        if(databaseHelper == null)
            databaseHelper = new DatabaseHelper(context, 1);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        query = query.toLowerCase();

        String[] allWords = query.split(" ");
        HashSet<Integer> taskIds = new HashSet<>(), actionIds = new HashSet<>();
        String appName = allWords[allWords.length - 1];
        int appId = getId(db, APPS_TABLE, APP_NAME, appName);
        if(appId == 0)
            return new ArrayList<>();

        //get rid of all ignores
        for(String word : allWords)
        {
            if(word == allWords[allWords.length - 1]) //skip the last word, because it's the app name
                continue;

            int wordTaskId = getId(db, TASKS_TABLE, TASK_NAME, word);
            int wordActionId = getId(db, ACTIONS_TABLE, ACTION_NAME, word);

            if(wordTaskId != 0)
                taskIds.add(wordTaskId);

            if(wordActionId != 0)
                actionIds.add(wordActionId);
        }

        if(taskIds.isEmpty() && actionIds.isEmpty())
            return new ArrayList<>();

        if(taskIds.isEmpty())
            for(int actionId : actionIds)
                taskIds.addAll(getFillerQueries(db, appId, 0, actionId));

        if(actionIds.isEmpty())
            for(int taskId : taskIds)
                actionIds.addAll(getFillerQueries(db, appId, taskId, 0));

        ArrayList<Query> validQueries = new ArrayList<>();
        for(int actionId : actionIds)
            for(int taskId : taskIds)
            {
                String action = getCorrespondingName(db, actionId, ACTIONS_TABLE, ACTION_NAME), task = getCorrespondingName(db, taskId, TASKS_TABLE, TASK_NAME);

                String solution = getSolution(db, actionId, taskId, appId);
                if(!solution.equals(ERROR_CODE))
                    validQueries.add(new Query(task, action, solution));
            }

        return validQueries;
    }

    private static int minimum(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }

    public static int levenshteinDistance(CharSequence lhs, CharSequence rhs)
    {
        int[][] distance = new int[lhs.length() + 1][rhs.length() + 1];

        for (int i = 0; i <= lhs.length(); i++)
            distance[i][0] = i;
        for (int j = 1; j <= rhs.length(); j++)
            distance[0][j] = j;

        for (int i = 1; i <= lhs.length(); i++)
            for (int j = 1; j <= rhs.length(); j++)
                distance[i][j] = minimum(
                        distance[i - 1][j] + 1,
                        distance[i][j - 1] + 1,
                        distance[i - 1][j - 1] + ((lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1));

        return distance[lhs.length()][rhs.length()];
    }
}