package com.catch22.savvy.activities;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;

import com.catch22.savvy.R;
import com.catch22.savvy.activities.tutorials.ScreenOne;
import com.catch22.savvy.activities.tutorials.TutorialActivity;
import com.catch22.savvy.backgroundProcesses.BackgroundService;
import com.catch22.savvy.backgroundProcesses.Constant;
import com.catch22.savvy.files.FileHandler;

public class SettingsActivity extends AppCompatActivity
{
    public static boolean notificationBarEnabled = true; //by default, enable the always running feature
    public static boolean internetServiceEnabled = false; //by default, keep everything on local database

    //the switches to toggle the Settings
    private Switch notificationsSwitch;
    private Switch accessInternetSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Get Permissions to read phone state
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT && !hasPermission())
                startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS), Constant.MY_PERMISSIONS_REQUEST_PACKAGE_USAGE_STATS);

        //Get all the settings data
        FileHandler.read(this);

        //Get all views
        notificationsSwitch = (Switch) findViewById(R.id.notificationSwitch);
        accessInternetSwitch = (Switch) findViewById(R.id.accessInternetSwitch);

        //set the check accordingly
        notificationsSwitch.setChecked(notificationBarEnabled);
        accessInternetSwitch.setChecked(internetServiceEnabled);

        notificationsSwitch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //set the boolean accordingly
                notificationBarEnabled = notificationsSwitch.isChecked();
                Log.d("Settings", "Notifications " + (notificationBarEnabled ? "Enabled" : "Disabled"));
                //write the new setting
                FileHandler.write(SettingsActivity.this);
            }
        });

        accessInternetSwitch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //set the boolean accordingly
                internetServiceEnabled = accessInternetSwitch.isChecked();
                Log.d("Settings", "Notifications " + (internetServiceEnabled ? "Enabled" : "Disabled"));
                //write the new setting
                FileHandler.write(SettingsActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == Constant.MY_PERMISSIONS_REQUEST_PACKAGE_USAGE_STATS && !hasPermission())
                startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS), Constant.MY_PERMISSIONS_REQUEST_PACKAGE_USAGE_STATS);
    }

    private boolean hasPermission()
    {
        AppOpsManager appOps = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
        int mode = 0;
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT)
            mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), getPackageName());
        return mode == AppOpsManager.MODE_ALLOWED;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            TutorialActivity.navigate(this, ScreenOne.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop()
    {
        super.onStop();

        if(SettingsActivity.notificationBarEnabled) //if the user has the notification bar settings enabled;
            startService(new Intent(SettingsActivity.this, BackgroundService.class)); //start the background process
        else
            stopService(new Intent(SettingsActivity.this, BackgroundService.class)); //stop the background process
    }
}