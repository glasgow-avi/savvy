package com.catch22.savvy.activities;

import android.app.Activity;
import android.os.Bundle;

import android.content.Intent;
import android.os.Handler;

import com.catch22.savvy.R;
import com.catch22.savvy.activities.tutorials.ScreenOne;
import com.catch22.savvy.db.DatabaseHelper;
import com.catch22.savvy.db.Inserter;
import com.catch22.savvy.files.FileHandler;

public class SplashScreen extends Activity
{
    public static DatabaseHelper databaseHelper;

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        /* New Handler to start the Menu-Activity 
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashScreen.this, FileHandler.exists(SplashScreen.this) ? SettingsActivity.class : ScreenOne.class);
                SplashScreen.this.startActivity(mainIntent);
                SplashScreen.this.finish();
            }

        }, SPLASH_DISPLAY_LENGTH);

        databaseHelper = new DatabaseHelper(this, 1);
        Inserter.insertAllEntries(databaseHelper);
    }
}