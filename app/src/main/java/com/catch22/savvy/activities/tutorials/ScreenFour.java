package com.catch22.savvy.activities.tutorials;

import android.os.Bundle;

import com.catch22.savvy.activities.SettingsActivity;
import com.catch22.savvy.files.FileHandler;

public class ScreenFour extends TutorialActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //get all the views
        super.onCreate(savedInstanceState);

        //set the instructions
        /*TODO: make this to .xml*/
        instructions.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        //the last tutorial screen will have the next button saying 'Got it' instead of Next
        nextButton.setText("Got it");

        //write the file to say I've opened the app already, don't show the tutorial again
        FileHandler.write(this);

        //set the previous activity as screen 3 and the next activity as the settings activity
        setPrevious(ScreenFour.this, ScreenThree.class);
        setNext(ScreenFour.this, SettingsActivity.class);
    }
}
