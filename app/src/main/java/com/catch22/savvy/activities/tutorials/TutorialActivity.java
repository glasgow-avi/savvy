package com.catch22.savvy.activities.tutorials;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.catch22.savvy.R;

/**
 * A class that defines a generic for all tutorial screens.
 * It's linked with a .xml file
 */
public abstract class TutorialActivity extends AppCompatActivity
{
    protected Button nextButton, backButton; //the next button which navigates to the next tutorial screen, or the Settings activity for the last tutorial screen
    protected ImageView image; //the image associated with the tutorial
    protected TextView instructions; //the instructions

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial); //link it to the XML file

        nextButton = (Button) findViewById(R.id.next_button);
        backButton = (Button) findViewById(R.id.back_button);
        image = (ImageView) findViewById(R.id.imageView);
        instructions = (TextView) findViewById(R.id.textView);
        //get all the views
    }

    /**
     * Navigates from one activity to the other given the source and the destination
     * Example: SourceClassName.this, DestinationClassName.class
     * @param source The source activity
     * @param destination The destination Class-Type
     */
    public static void navigate(final Activity source, final Class<?> destination)
    {
        //sends an intent
        Intent nextIntent = new Intent(source, destination);
        source.startActivity(nextIntent); //start the destination activity
        source.finish(); //end the source activity
    }

    /**
     * Sets the on click listener of a button to navigate from one activity to the other
     * Example: buttonInstance, SourceClassName.this, DestinationClassName.class
     * @param button The button to click
     * @param source The source activity
     * @param destination The destination Class-Type
     */
    private void setNavigation(Button button, final AppCompatActivity source, final Class<?> destination)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //navigate the source to destination
                navigate(source, destination);
            }
        });
    }

    /**
     * Sets the on click listener of the next button, given the source class name and the destination class name
     * @param source The source activity
     * @param type The destination Class-Type
     */
    protected void setNext(final AppCompatActivity source, final Class<?> type)
    {
        setNavigation(nextButton, source, type);
    }

    /**
     * Sets the on click listener of the back button, given the source class name and the destination class name
     * @param source The source activity
     * @param type The destination Class-Type
     */
   protected void setPrevious(final AppCompatActivity source, final Class<?> type)
    {
        setNavigation(backButton, source, type);
    }
}