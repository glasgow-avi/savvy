package com.catch22.savvy.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.catch22.savvy.R;
import com.catch22.savvy.backgroundProcesses.BackgroundService;
import com.catch22.savvy.db.StringHandler;
import com.catch22.savvy.files.FileHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static com.catch22.savvy.db.DatabaseHelper.ERROR_CODE;

/**
 * The Activity that opens on Tapping savvy from the drop down menu, appears as a dialog box
 */
public class PopupActivity extends Activity
{
    public static String userInputQueryString; //the query that the user enters
    public static String lastApplicationName; //the name of the last Application the user was in
    public static String solution = ""; //the solution to the query of the user

    private static final String API_KEY = "&apikey=240b9794-10cf-4371-afdd-3f2fdad16080";
    private static final String API_URL = "https://api.havenondemand.com/1/api/sync/extractconcepts/v1?text=";
    private static String url;
    private static String jsonStr = "";


    private SearchView queryEditor; //where you type your question into
    private TextView solutionResult; //where it displays the solution
    private TextView loadPreviousResultButton; //button to load the previous result
    private Button okButton; //the ok button which you click to search your query on google when the database string matching fails

    private boolean solutionSelected = false;

    private ListView validQueryListView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);

        solutionResult = (TextView) findViewById(R.id.solutionResult);
        queryEditor = (SearchView) findViewById(R.id.editText);
        loadPreviousResultButton = (TextView) findViewById(R.id.loadPreviousResultButton);
        okButton = (Button) findViewById(R.id.searchOnGoogleButton);
        validQueryListView = (ListView) findViewById(R.id.validQueryListView);

        validQueryListView.setVisibility(View.GONE);

        queryEditor.setIconifiedByDefault(false); //by default, you can type in text, you don't have to click on the search button


        okButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                searchGoogle(userInputQueryString + " " + lastApplicationName); //search the query on google, plus the last Application name
            }
        });

        queryEditor.setOnQueryTextListener(new SearchView.OnQueryTextListener() //set the listener for enter key pressed on the query
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                String lastActivity = BackgroundService.lastActivePackage;
                String[] packageNameParts = lastActivity.split("\\.");

                userInputQueryString = queryEditor.getQuery().toString();
                lastApplicationName = packageNameParts[packageNameParts.length - 1];
                ArrayList<StringHandler.Query> solutions;

                query = userInputQueryString + " " + lastApplicationName;

                boolean useApi = SettingsActivity.internetServiceEnabled && BackgroundService.isNetWorkAvailable;

                if(useApi && false) //TODO: Fix API
                {
                    //use the API
                    userInputQueryString = userInputQueryString.replace(" ","+");
                    new AsyncCaller().execute();
                    query = userInputQueryString + lastApplicationName;
                }

                solutions = StringHandler.stripQuery(PopupActivity.this, query);

                if(solutions.equals(ERROR_CODE + "1"))
                    while(!solutionSelected);

                queryEditor.setVisibility(View.GONE);

                solution = selectSolution(solutions);

                if(solutions.size() > 1)
                    return true;

                //show the solution and hide everything else
                showSolution();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        loadPreviousResultButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //hide everything, but show the solution
                queryEditor.setVisibility(View.GONE);
                solutionResult.setVisibility(View.VISIBLE);
                v.setVisibility(View.GONE);

                FileHandler.read(PopupActivity.this); //read the last result from a file
                solutionResult.setText(solution); //set the text of the solution
            }
        });
    }

    private void showSolution()
    {
        solutionResult.setVisibility(View.VISIBLE);
        validQueryListView.setVisibility(View.GONE);
        loadPreviousResultButton.setVisibility(View.GONE);

        if(solution.equals(ERROR_CODE))
        {
            //no solution found
            solution = ""; //reset the solution string
            solutionResult.setText("We don't have the answer to that. Would you like to Google it instead?"); //set the text of the solution to uh... I don't know?
            okButton.setVisibility(View.VISIBLE); //set the oK button to visible
            return;
        }

        solutionResult.setText(solution); //set the text of the solution to the actual solution
        FileHandler.write(PopupActivity.this); //write the file for purposes of Load Previous result
    }

    private String selectSolution(ArrayList<StringHandler.Query> queries)
    {
        solutionSelected = false;

        switch (queries.size())
        {
            case 1:

                return queries.get(0).solution;


            case 0:

                return ERROR_CODE;
        }

        validQueryListView.setVisibility(View.VISIBLE);

        validQueryListView.setAdapter(new ArrayAdapter<>(this, R.layout.list_item, queries));

        validQueryListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                solution = ((StringHandler.Query) parent.getItemAtPosition(position)).solution;
                showSolution();
            }
        });

        return null;
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        queryEditor.setVisibility(View.VISIBLE); //by default, you can see the query editor
        solutionResult.setVisibility(View.GONE); //by default, you can't see the result
        okButton.setVisibility(View.GONE); //by default, you can't see the search on google button
        loadPreviousResultButton.setVisibility(View.VISIBLE); //by default, you can see the load previous result button
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_popup, menu);
        return true;
    }

    private class AsyncCaller extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params)
        {
            try
            {
                url = API_URL + userInputQueryString + API_KEY;
                Log.d("URL->", url);
                URL website = new URL(url);
                Log.d("After before", "getJsonString ");
                URLConnection connection = website.openConnection();
                Log.d("After open", "getJsonString ");
                connection.setRequestProperty("Accept-Charset", "UTF-8");
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String inputLine;

                while ((inputLine = in.readLine()) != null)
                    response.append(inputLine);

                in.close();


                jsonStr = response.toString();
                Log.d("response", "doInBackground " + jsonStr);

            } catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            Log.d("Response: ", "> " + jsonStr);
            if(jsonStr.equals(""))
                Toast.makeText(getApplicationContext(), "Connection Unsuccessful", Toast.LENGTH_SHORT).show();
            else
            {
                Toast.makeText(getApplicationContext(), "Connection Successful", Toast.LENGTH_SHORT).show();
                try
                {
                    JSONObject jsonObject = new JSONObject(jsonStr);
                    JSONArray jsonArray = jsonObject.getJSONArray("concepts");
                    userInputQueryString = "";
                    for(int  i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject content = jsonArray.getJSONObject(i);
                        userInputQueryString += content + " ";
                        Log.d("KEYWORDS->",userInputQueryString);
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                jsonStr = "";
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Opens a browser window
     * Searches a given string on Google
     * @param query The given String
     */
    public void searchGoogle(String query)
    {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://www.google.co.in/search?q=" + query.replace(" ", "+")));
        startActivity(i);
    }
}